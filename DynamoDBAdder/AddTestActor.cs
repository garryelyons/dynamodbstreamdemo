﻿using Amazon.DynamoDBv2.DataModel;
using AwsDynamoDBDataModelSample1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamoDBDataModelSample1
{
    public class AddTestActor
    {
        public void AddActor(DynamoDBContext context, string actorName)
        {

            Actor TestActor = new Actor
            {
                Name = actorName,
                Bio = actorName,
                BirthDate = new DateTime(1933, 3, 14),
                Address = new Address
                {
                    City = "London",
                    Country = "England"
                },
                HeightInMeters = 1.88f
            };

            

            Console.WriteLine("Saving actor: " + actorName);
            context.Save<Actor>(TestActor);
        }

        public void AddMovie(DynamoDBContext context, string movieName)
        {
            Movie testMovie = new Movie
            {
                Title = movieName,
                Genres = new List<string> { "Drama" },
                ReleaseDate = DateTime.Today,
                ActorNames = new List<string>
                {
                    "Juan Diego"
                }
                
            };
            context.Save<Movie>(testMovie);
        }

        public void AddTestperson(string personid)
        {

        }
    }

    
}
